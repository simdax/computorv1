from lexer import GRAMMAR

class ARITHMETIC(GRAMMAR):
    def __init__(self):
        self.operators = {
            '+': (lambda a, b: a + b, 0),
            '-': (lambda a, b: a - b, 0),
            '*': (lambda a, b: a * b, 1),
            '/': (lambda a, b: a / b, 1),
            '^': (lambda a, b: a ** b, 2),
# this lines are useless, its only to see if some features works
            '!': (lambda a: (print(a), a)[1], 3),
            'zero': (lambda: 0, 99),
        }
