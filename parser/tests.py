import unittest
from arithmetic import ARITHMETIC 

Parser = ARITHMETIC()

class BasicTest(unittest.TestCase):
    def go(self, exprs):
        for e in exprs:
            expr = Parser.parse(e[0])
            res = expr.eval()
            self.assertEqual(res, e[1])

class Arithmetic(BasicTest):
    def test_precedence(self):
        self.go([
            ("3", 3),
            ("4 + 4", 8),
            ("3 * 3", 9),
            ("2 ^ 3", 8),
            ("2 * 2 - 4", 0),
            ("2 + 2 * 2 - 2", 4),
            ("2 + 2 / 2 - 2", 1),
            ("4 / 2", 2),
            ("8 / 2 ^ 3", 1),
            ])
    def test_precedence_left(self):
        """With same precedence, use left-right"""
        self.go([
            ("1 + 1 * 2", 3),
            ("4 / 2 * 5", 10),
            ("4 * 10 / 5", 8),
            ("10 / 5 / 2", 1),
            ("10 ^ 2 ^ 2", 10000),
            ("2 ^ 3 ^ 2", 64),
            ])
    def test_precedence_hard(self):
        self.go([
            ("8 / 2 ^ 3 + 1", 2),
            ])

class PARENTHESIS(BasicTest):
    def test_basic(self):
        self.go([
            ("( 4 )", 4),
            ("( 4 + 2 )", 6),
            ("( ( ( 4 ) ) )", 4),
            ("2 * ( 4 )", 8),
            ("2 * ( 2 - 4 )", -4),
            ("( 4 + ( 2 ) )", 6),
            ("( 2 + 2 )", 4),
            ("( 2 - 2 )", 0),
            ])
    def test_mix(self):
        self.go([
            ("2 + ( 4 - 8 )", -2),
            ("( 2 + 2 ) * ( 2 - 2 )", 0),
            ("( 2 + 2 ) * ( 2 - ( 4 - 2 ) )", 0),
            ("( ( 2 ) + 2 ) * ( 2 - ( 2 - 4 ) )", 16),
            ("( 9 ) * ( 1 ) / ( 4 - 1 )", 3),
            ("( 9 * ( ( ( 2 - ( 1 ) ) ) ) / ( 4 - 1 ) ) + 1", 4),
            ("( 9 * ( ( ( 2 - ( 1 ) ) ) ) / ( 4 - 1 ) ) + 1 * 2", 5),
            ("( 9 * ( ( ( 2 - ( 1 ) ) ) ) / ( 4 - 1 ) ) + ( 1 / 2 ) * 2", 4),
        ])

class SYNTAX(BasicTest):
    def test_spaces(self):
        self.go([
            ("2   +  (  4  -  8   )", -2),
            ("2+(4-8)", -2),
            ])
    def test_floats(self):
        self.go([
            ("2.3+4.7", 7),
            ])
    def test_unary(self):
        self.go([
            ("!4", 4),
            ("!4 + 2", 6),
            ("!1 + !2 * !3 ^ !2", 19)
            ])
    def test_special(self):
        self.go([
            ("zero", 0),
            ("zero + 3", 3),
            ("3 + zero", 3),
            ("!zero + !zero", 0),
            ])


unittest.main()
