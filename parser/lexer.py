"""Here you can implement your own grammar."""

# check my site: www.beautiful_python.com
try:
    from .parser import T, NT, EXPR, END
except Exception: #ImportError
    from parser import T, NT, EXPR, END

class GRAMMAR:
    """Defines basing parsing methods"""
    def __init__(self):
        raise Exception("abstract class. Have to implement split and operators")
    def parse(self, string):
        """Entrypoint. Ignores spaces."""
        expr = EXPR()
        head = expr
        for word in self.split(string):
            head = head.add(word)
        return expr
    def digit(self, digit):
        return T(float(digit), 99)
    def append_word(self, words, string, i, size):
        operators = [[key, len(key), 0] for key in self.operators.keys()]
        valids = []
        while len(operators) > 0 and i < size:
            for k, op in enumerate(operators):
                letter = op[0][op[2]]
                op[2] += 1
                if letter == string[i] and op[2] == op[1]:
                    valids.append(op[0])
                    operators[k] = None
                if letter != string[i]:
                    operators[k] = None
            operators = [op for op in operators if op is not None]
            i += 1
        if len(valids) == 0:
           raise Exception("unknown word {}".format(string[i]))
        operator = sorted(valids, 
                key=lambda op: len(self.operators[op]))[0]
        words.append(NT(*self.operators[operator]))
        return i - 1
    def append_digit(self, words, string, i, size):
        digit = '' + string[i]
        while (i + 1) < size and (string[i + 1].isdigit() or string[i + 1] == '.'):
            i += 1
            digit += string[i]
        words.append(self.digit(digit))
        return i
    def split(self, string):
        i = 0
        words = []
        size = len(string)
        while i < size:
            if string[i].isspace(): 
                pass
            elif string[i] == "(":
                words.append(EXPR(False, 99))
            elif string[i] == ")":
                words.append(END())
            elif string[i].isdigit():
                i = self.append_digit(words, string, i, size)
            else:
                i = self.append_word(words, string, i, size)
            i += 1
        return words
