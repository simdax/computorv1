class NODE:
    """binary tree Trait class"""
    def place(self, value):
        """add a value as child of self"""
        self.args.append(value)
        value.parent = self
        return value
    def replace(self, value):
        """insert a value as parent of self"""
        parent = value.parent
        parent.args.remove(value)
        self.place(value)
        return parent.place(self)
    def up(self, predicate):
        """go up to search a child"""
        child = self
        while predicate(child.parent):
            child = child.parent
        return child
    def show(self):
        """Print tree"""
        try:
            print(inspect.getsource(self.value))
        except:
            print(self.value)
        for arg in self.args:
            arg.show()

class EXPR(NODE):
    """Node with a specific 'add' behaviour, according to nature of value to insert"""
    def __init__(self, value=True, weight=-1):
        self.parent = None
        self.weight = weight
        self.args = []
        self.value = value
    def eval(self):
        """Eval child"""
        if not self.value:
            raise Exception("no closing par")
        return self.args[0].eval()
    def add(self, value):
        """Handles insertion according to weight"""
        if value.__class__ == END:
            return value.close(self)
        if value.weight < self.weight:
            child = self.up(lambda parent: (
                parent is not None
                and parent.__class__ is not EXPR
                and value.weight <= parent.weight))
            return value.replace(child)
        return self.place(value)

class END(EXPR):
    """Handles closing Parenthesis"""
    def close(self, value):
        """go up until it finds the beginning expression"""
        #if (value.__class__ is EXPR):
        #    raise Exception("empty expression ?")
        child = value.up(lambda parent: parent.__class__ is not EXPR)
        if child.parent is None:
            raise Exception("no opening par")
        child.parent.value = True
        return child.parent

class T(EXPR):
    """Terminal. No args."""
    def place(self, value):
        raise Exception("impossible")
    def eval(self):
        return self.value

class NT(EXPR):
    """Non-Terminal. Requires args."""
    def eval(self):
        return self.value(*[arg.eval() for arg in self.args])
