def sqrt(x):
    neg = x < 0
    x = abs(x)
    last_guess = x / 2.0
    while True:
        guess = (last_guess + x/last_guess) / 2
        if abs(guess - last_guess) < .000001:
            if neg:
               return -guess
            return guess
        last_guess = guess
  
def solve(a, b):
    solution = [0, 0, 0, 0, 0]
    i = 0
    while i < 3:
        solution[i] = a.exponents[i] - b.exponents[i]
        i += 1
    degree = solution.index(max(solution))
    print("Polynomial degree: {}".format(degree))
    c, b, a, *rest = solution
    print("Reduced form: {} + {} X + {} X^2 = 0".format(*solution))
    if degree > 2:
        print("The polynomial degree is strictly greater than 2, I can't solve.")
        return
    elif degree < 2:
        return
     
    discriminant = sqrt((b * b) - (4 * a * c))
    print("the discriminant is:", discriminant)
    if discriminant == 0:
        print("The solution is: {}".format(-b / (2 * a)))
    elif discriminant > 0:
        print("The solutions are: \n{}\n{}".format(
                -b / (2 * a) + (discriminant / (2 * a)),
                -b / (2 * a) - (discriminant / (2 * a)),
            ))
    else:
        print("The solution is: {} ± {}i".format(
            -b / (2 * a),
            discriminant / (2 * a)
        ))
