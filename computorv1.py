from parser.parser import T
from parser.lexer import GRAMMAR
from maths import solve

class Polynom:
    def __init__(self):
        self.exp = 1
        self.exponents = [0, 0, 0, 0, 0]
    def setExponent(self, exp):
        if (exp > 5):
            raise Exception("woaw, {} is way too big for an exponent!".format(exp))
        if (exp < 0):
            raise Exception("woaw, {} is negative, fucking shit !".format(exp))
        self.exp = int(exp)
        return self
    def set(self, value):
        self.exponents[self.exp] = value
        return self
    def mul(self, value):
        self.exponents[self.exp] *= value
        return self

class COMPUTORV1(GRAMMAR):
    def digit(self, digit):
        return T(Polynom().setExponent(0).set(float(digit)), 99)
    def __init__(self):
        self.operators = {
                '=': (solve, 0),
                '+': (self.plus, 1),
                '-': (self.moins, 1),
                '*': (self.mul, 2),
                '^': (self.exp, 3),
                'X': (lambda: Polynom().setExponent(1).set(1), 99),
                }
    def mul(self, a, b):
        return b.mul(a.exponents[0])
    def exp(self, a, b):
        a.exponents[1] = 0
        return a.setExponent(int(b.exponents[0])).set(1)
    def moins(self, a, b):
        return self.plus(a, b, False);
    def plus(self, a, b, pos = True):
        i = 0
        while i < 3:
            if pos:
                a.exponents[i] += b.exponents[i]
            else:
                a.exponents[i] -= b.exponents[i]
            i += 1
        return a 
