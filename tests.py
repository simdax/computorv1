from computorv1 import COMPUTORV1

expressions = [
"5 * X^0 + 4 * X^1 - 9.3 * X^2 = 1 * X^0",
"1 * X^0 + 2 * X^1 + 5 * X^2 = 0",
"X + 1 = 1",
"2 * X + 1 = 7",
        ]

prog = COMPUTORV1()

for expr in expressions:
    print("testing :", expr)
    e = prog.parse(expr).eval()
